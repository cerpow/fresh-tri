//LOAD ANIMATION
var count = 1;
var clicked = false;
var counter = $('.lottie-count');
var anim = lottie.loadAnimation({
	container: $('.lottie')[0],
	path: 'data.json',
	renderer: 'svg',
	loop: true,
	autoplay: false
});

//LOOP BREATHING
anim.addEventListener('DOMLoaded', function() {
	anim.playSegments([0, 120], true);
	counter.addClass('breathing');
});

//CLICK ANIMATION
$('.lottie').click(function() {
	clicked = true;
	anim.playSegments([120, 210], true);
	increaseTri();
});

//RESET ANIMATING AFTER CLICK
anim.addEventListener('loopComplete', function() {
	if (clicked) {
		anim.playSegments([0, 120], true);
		animating = false;
		counter.addClass('breathing');
	}
});

//COUNTER
function increaseTri() {
	count++;

	counter.fadeOut(250, function() {
		counter.removeClass('breathing');
		$('.lottie-count').text(count);
		counter.fadeIn(250);
	});
}
